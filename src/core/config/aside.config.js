// import store from "@/core/services/store";

const Menu = [
  {
    url: "/dashboard",
    title: "Dashboard",
    icon: "flaticon2-architecture-and-city",
    module: "dashboard",
  },
  {
    url: "/delivery",
    title: "Delivery",
    icon: "flaticon2-delivery-truck",
    submenu: [
      {
        url: "/order",
        title: "Order",
        module: "order",
      },
      {
        url: "/tarif",
        title: "Tarif",
        module: "tarif",
      },
    ],
  },
  {
    url: "/user-management",
    title: "Manajemen User",
    icon: "flaticon2-avatar",
    submenu: [
      {
        url: "/user",
        title: "User",
        module: "user",
      },
      {
        url: "/zone",
        title: "Wilayah",
        module: "zone",
      },
      {
        url: "/role",
        title: "Role",
        module: "role",
      },
      {
        url: "/module",
        title: "Modul",
        module: "module",
      },
      {
        url: "/action",
        title: "Tindakan",
        module: "action",
      },
    ],
  },
  {
    url: "/files",
    title: "Pengaturan Dokumen",
    icon: "flaticon-file-1",
    submenu: [
      {
        url: "/file",
        title: "Dokumen",
        module: "file",
      },
      {
        url: "/file-category",
        title: "Kategori",
        module: "file-category",
      },
    ],
  },
];

export default Menu;
